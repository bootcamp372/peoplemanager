﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Person : IComparable<Person>
    {
        public string Name { get; set; }
        public int Age { get; private set; }

        public string Address { get; private set; }

        public string City { get; private set; }

        public string State { get; private set; }
        public string ZipCode { get; private set; }
        public Person(string name, int age, string address, string city, string state, string zipCode)
        {
            Name = name;
            Age = age;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipCode;
        }

        public class Worker : Person
        {
            public string JobTitle { get; set; }
            public decimal Salary { get; set; }

            public Worker(string name, int age, string address, string city, string state, string zipCode, string jobTitle, decimal salary) :base(name, age, address, city, state, zipCode)
            {
                JobTitle = jobTitle;
                Salary = salary;

            }
            public override void Display()
            {
                Console.WriteLine($"{Name} is {Age}");
                Console.WriteLine("Address:");
                Console.WriteLine($"{Address}");
                Console.WriteLine($"{City},  {State}");
                Console.WriteLine($"{ZipCode}");
                Console.WriteLine($"Job Title: {JobTitle}");
                Console.WriteLine($"Salary: {Salary}");
                Console.WriteLine("--------------");
            }
        }

        public void HaveABirthday()
        {
            Age++;
            Console.WriteLine($"It is {Name}'s birthday today. The new age is: {Age}");
            Console.WriteLine("--------------");
        }
        public void Move()
        {
            Console.WriteLine($"Change address for {Name}:");
            string newAddress = Console.ReadLine();
            Console.WriteLine("Enter a new city:");
            string newCity = Console.ReadLine();
            Console.WriteLine("Enter a new state:");
            string newState = Console.ReadLine();
            Console.WriteLine("Enter a new zipcode:");
            string newZip = Console.ReadLine();

            Address = newAddress;
            City = newCity;
            State = newState;
            ZipCode = newZip;
        }
        public virtual void Display()
        {
            Console.WriteLine($"{Name} is {Age}");
            Console.WriteLine("Address:");
            Console.WriteLine($"{Address}");
            Console.WriteLine($"{City},  {State}");
            Console.WriteLine($"{ZipCode}");
            Console.WriteLine("--------------");
        }

        public int CompareTo(Person? other)
        {
            return Name.CompareTo(other.Name);
        }

        /*        public int CompareTo(Person other)
       {
           return Name.CompareTo(other.Name);
       }*/
        // descending sort by age
        public class DescendingAgeSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return y.Age - x.Age;
            }
        }

        public class AscendingNameSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                return x.Name.CompareTo(y.Name);
            }
        }

        public class AscendingStateCitySorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                int compareInt = String.Compare(x.State, y.State, comparisonType: StringComparison.OrdinalIgnoreCase);
                if (compareInt != 0) { 

                    return compareInt; 
                }
                else { return String.Compare(x.City, y.City, comparisonType: StringComparison.OrdinalIgnoreCase); ; }
                
            }
        }

        public class DescendingZipSorter : IComparer<Person>
        {
            public int Compare(Person x, Person y)
            {
                int xVal = Convert.ToInt32(x.ZipCode);
                int yVal = Convert.ToInt32(y.ZipCode);
                return yVal - xVal;
            }
        }

    }
}
