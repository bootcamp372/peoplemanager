﻿using static PeopleManager.Person;

namespace PeopleManager
{

    internal class Program
    {
        static void Main(string[] args)
        {
            // create list of persons
            // call add birthday method
            // call Move()
            // sort list of people
            // call display
            List<Person> people = new List<Person>();
            people.Add(new Person("Ian Thomas", 17, "1234 Main St", "Detroit", "MI", "48226"));
            people.Add(new Person("Thaddeus Danger", 27, "1234 1st St", "Temeculah", "AZ", "38526"));
            people.Add(new Person("Ezra Smith", 18, "1234 Layfayette", "Lincoln Park", "MI", "48146"));
            people.Add(new Person("Jon Doe", 56, "1234 Capitol", "Allen Park", "MI", "48146"));
            people.Add(new Person("Pursalane Smith", 43, "1234 6th St", "Chicago", "IL", "39527"));
            people.Add(new Person("Alice Jones", 11, "1234 6th St", "Juneau", "AK", "22748"));
            people.Add(new Worker("Cleofira Cojacari", 21, "18 Nevada St", "Souix City", "SD", "22748", "Seamstress", 35000));
            people.Add(new Worker("Constantin Cojacari", 31, "1234 6th St", "Washington", "WA", "22748", "Bartender", 45000));

            people[0].Move();
            people[1].HaveABirthday();
            people.Sort(new AscendingNameSorter());
            /*            people.Sort(new DescendingAgeSorter());
                        people.Sort(new DescendingZipSorter());*/
            people.Sort(new AscendingStateCitySorter());

            foreach (Person person in people)
            {
                person.Display();
            }
        }
    }
}